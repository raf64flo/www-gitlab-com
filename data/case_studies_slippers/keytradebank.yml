title: Keytrade Bank
file_name: keytradebank
canonical_path: /customers/keytradebank/
cover_image: /images/blogimages/keytradebank_cover.jpg
cover_title: |
  Keytrade Bank centralizes its tooling around GitLab
cover_description: |
  Keytrade Bank adopted GitLab for code management, version control, and CI/CD, replacing four tools and improving workflow efficiency.
twitter_image: /images/blogimages/keytradebank_cover.jpg
twitter_text: Learn how Keytrade Bank improved version control, CI/CD, and cross-company collaboration with GitLab
customer_logo: /images/case_study_logos/keytrade.png
customer_logo_css_class: brand-logo-tall
customer_industry: Financial Services
customer_location: Belgium
customer_solution: GitLab Premium
customer_employees: 250 
customer_overview: |
  Keytrade Bank improved version control, CI/CD, and cross-company collaboration with GitLab.
customer_challenge: |
  Keytrade Bank was looking for a solution to simplify workflow, improve visibility, cross-team collaboration, and provide superior customer satisfaction.
key_benefits: |-


    AWS integration

  
    Improved code management

  
    End-to-end visibility

  
    Cloud integration

  
    Jira integration 

  
    Improved customer communication and satisfaction

customer_stats:
  - stat: 4  
    label: Tools (BitBucket, SVN, Jenkins, and Openshift) replaced by GitLab
  - stat: |
      1,000     
    label: Jobs executed daily 
  - stat: 500     
    label: Projects since adoption
customer_study_content:
  - title: the customer
    subtitle: Online financial institution
    content: >-
    
  
        Keytrade Bank is the Belgian market leader in online banking and trading, offering a full range of products and services on its digital platforms. It is part of the Crédit Mutuel Arkéa Group. At [Keytrade Bank](https://www.keytradebank.be/) consumers can find everything they want from a bank (current accounts, savings accounts, cards, mortgage loans, trading and investment possibilities etc.) without ever setting foot in one. The bank helps its clients reach total independence in managing their banking and investment transactions, 24/7.
      
  - title: the challenge
    subtitle: Managing too many tools 
    content: >-
    
  
        Keytrade Bank was using several different tools within the workflow chain from development to production. On top of that, the teams had created internal scripts for specific needs. With various tools in place, the teams were constantly context switching from one tool to another. Each tool had its own problems and the experts of those tools spent too much time managing and maintaining them. These siloed tools resulted in lots of manual processes, lots of blackboxes, and a lack of visibility across the workflow.  
    
  
        Managers were also lacking the ability to audit work, in terms of who was responsible for which parts of a project and how and when it was happening. It is very important for management to understand what everyone is working on and it was nearly impossible with the existing level of visibility across the toolchain. “We would also have had a lot more finger pointing internally between Dev and Ops, trying to establish what’s going on and where the problem is really coming from. Now, it’s much easier to identify the problems and know how to solve them,” according to Niels Peto, DevOps and Cloud Engineer. The team was looking for a way to solve these problems and to avoid errors in communication, both internally and with customers. Customer communication is a top priority and Keytrade Bank was looking for the best solution to provide customers with superior service.

  - title: the solution
    subtitle: One platform, lots of features
    content: >-
    
  
        “We have tried other suppliers before, but none of them had the support and level of automation of GitLab,” Peto said. GitLab provides developers with a [single solution](/topics/single-application/) that provides several features. “With GitLab, we really have a solution for the development part to the production part. It allows us to do exactly what we did before, or even more because it offers more features, which is really great and that is the main reason for our change,” said Nicolas Pepinster, DevOps and Cloud Engineer. “GitLab is constantly developing and often bringing new features, which we are fully satisfied with.”
    
  
        The teams were using Nexus for artifact management presently and are reviewing how GitLab registry could fully replace this tool in the future. In addition, teams were previously using OpenShift container registry and are now using the integrated GitLab registry to store their container images using an AWS S3 backend. GitLab has enabled them to offer an integrated single solution and reduce toolchain complexity. 

  - title: the results
    subtitle: Version control, visibility, and collaboration
    content: >-
    
  
        Keytrade Bank now uses GitLab for its Bank Assembly platform. Each team at Keytrade Bank works on different products supplied by the bank using their own set of applications. Bank Assembly has been built from scratch with the aim to provide a transversal platform based on several tools, and they specifically chose GitLab for the center of the platform. The Bank Assembly has a set of built-in pipelines. Teams can use what they need in the blocks because they’ve created all the ymls in the repository and the applications make e-clouds out of the ymls. It is easy to switch from one version to another because it all goes through GitLab. 
    
  
        “All the teams are starting to be embedded in the Bank Assembly platform, and so we use GitLab as a central point for everything CI/CD, infrastructure, on-premise integration, and AWS,” according to Pepinster. Keytrade Bank installed [GitLab server on AWS](https://docs.gitlab.com/ee/install/aws/) using omnibus. They configured an external PostgreSQL with Aurora RDS. For GitLab runners, teams deployed them both on-premises and in AWS. On AWS, GitLab runners are EC2 instances that spawn new EC2 spot-instances when they pick-up a job from GitLab server. It enables teams to handle loads efficiently without thinking about the capacity of the infrastructure. As GitLab runners are deployed in dedicated per feature team AWS accounts, they can easily adjust the setup according to their needs.   
    
  
        Keytrade Bank applications are deployed in AWS mainly on EKS using GitLab. Teams also use Lambda to deploy a simple dashboard showing some crucial information about their applications. As for the other technologies, GitLab is used to deploy Lambda and Terraform configuration related to it. Since adopting GitLab, managers are able to properly audit workflows. “We can now see who does what, and when, which was not the case before. We have a lot of auditors who come to the bank, and it is very important for our management to be able to have all this information - it is now very easy for us,” Pepinster said.
    
  
        GitLab is also used by the Contact Management Development team. They are responsible for customer management and are the main communication channel with customers. This team is an extremely important element for the bank because there are no physical branches. Keytrade Bank is integrating GitLab with Jira in order to allow the business to control the complete software delivery lifecycle from Jira. In Jira, they developed a workflow with a set of statuses and transitions that a task moves through during its lifecycle. A new branch will trigger a transition to move the task from "open" all the way through to “code to be reviewed.” Triggering this transition automatically opens a new merge request in GitLab, using the source branch created before. Review is done in GitLab by other developers and the result of the merge request (either close or merged) triggers another Jira transition.
    
  
        The same process happens for deployment: Only authorized users can trigger the transition "to be deployed in prod." This creates a Git tag and triggers the corresponding pipeline to release an application in production. Keytrade Bank’s newest project, PSD2, is a set of APIs that must be exposed publicly according to a European standard. PSD2 was created directly with GitLab from its inception. The project uses cloud, on-premise, AWS, and Terraform, and all the tools are managed by GitLab. PSD2 has around 10 microservices, two third-parties on the cloud, and one on-premise tier. “The idea is to be able to reuse what has been done for Belgium for future projects. For example, for the implementation in Luxembourg. The PSD2 project was the first big project we used GitLab for and it went really well from development to production,” Pepinster said.
    
  
        GitLab is the foundation for communication, collaboration, and workflow efficiency at Keytrade Bank. As the project expands, GitLab will continue to be the hub for code management, version control, and continuous integration and deployment.
      
    
  
        ## Learn more about GitLab Premium
    
  
        [Why source code management matters](/stages-devops-lifecycle/source-code-management/)
    
  
        [The benefits of GitLab CI](/features/continuous-integration/)
    
  
        [Choose a plan that suits your needs](/pricing/)
customer_study_quotes:
  - blockquote: GitLab allows us to have a common tool between Dev and Ops. DevOps is really a mentality, and you really try to put it in place within the company, so that people fit into that culture. GitLab really allows it because it’s a common solution between these two teams, which we didn’t have before. It really simplified the workflow at that level.
    attribution: Nicolas Pepinster 
    attribution_title: DevOps and Cloud Engineer    
