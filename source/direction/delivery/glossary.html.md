---
layout: markdown_page
title: "Glossary of delivery terminology"
---

# Goals

This glossary is meant to serve as a guide to help team members and users discuss topics related to the Ops sub-department and more specifically application delivery. It aims to achieve the following:  

* Align concept definitions to improve the effectiveness of communication between Ops team members.
* Reduce the potential for miscommunication.
* Help new team members and community contributors get up to speed faster, reducing the time to productivity.

# Scope

The terms and their definitions outlined in this document are provided in context specifically for the GitLab product. Therefore, these terms may have different meanings to users outside of GitLab.

## Relationships at a glance

[![](https://mermaid.ink/img/pako:eNqFkk1PwzAMhv9KlOs-xK65TdsOCCEkhjigXkzidtHSuHISYEz772RdywpsIqfEeV7nje291GRQKom8tFAx1IUXea38m2XyNfooZmIyETcjscTG0e4YUmID4Qo4Ewvypa0SQ7TkB-iPeAePxB3unsElHIDnh47AdJrZedM4q7uMpr3v4MHNfwYueH0CrjDe-pIhRE46Js5OQGsMAa_buSzDDxtiePB_jd2cdCMx52hL0LmC1muXTP9GH-99ramM78C4yN1RogG9hapnH8k5Sq2Zlh18S4lXdOSrXyR5FMTiE5lOkmErO4Ucyxq5BmvyNOyP-kLGDdZYSJW3BnhbyMIfMpcaAxFXxkZiqUpwAccSUqT1zmupckGwh7qh-qYa8C9E5zO2Se5PM9iO4uELMHrZtg?type=png)](https://mermaid.live/edit#pako:eNqFkk1PwzAMhv9KlOs-xK65TdsOCCEkhjigXkzidtHSuHISYEz772RdywpsIqfEeV7nje291GRQKom8tFAx1IUXea38m2XyNfooZmIyETcjscTG0e4YUmID4Qo4Ewvypa0SQ7TkB-iPeAePxB3unsElHIDnh47AdJrZedM4q7uMpr3v4MHNfwYueH0CrjDe-pIhRE46Js5OQGsMAa_buSzDDxtiePB_jd2cdCMx52hL0LmC1muXTP9GH-99ramM78C4yN1RogG9hapnH8k5Sq2Zlh18S4lXdOSrXyR5FMTiE5lOkmErO4Ucyxq5BmvyNOyP-kLGDdZYSJW3BnhbyMIfMpcaAxFXxkZiqUpwAccSUqT1zmupckGwh7qh-qYa8C9E5zO2Se5PM9iO4uELMHrZtg)

## List of terms


### Application

An application consists of one or more artifacts and a set of configurations. The artifacts likely package software code directly or as embedded/referenced artifacts. The configurations might be connected to one or more released versions.

Some of the artifacts in a release might come from an application, but a release might contain artifacts that are not an application.

There is always only 1 copy of each application in an environment.

Nota bene, that a single GitLab project might host multiple applications (e.g: `kas` and `agentk` are hosted in a single repo).

### Artifact

An artifact is the result of a set of actions, typically identified by an address or an ID in the system. It's considered immutable and must contain the following cryptographically signed attributes: 

A list of all the sources and tools that were used to produce it for traceability.
* Addresses (e.g. URLs) of binaries that were output artifacts. This includes the size and cryptographic hash of the contents in each file.
* Links to source repo(s) and builds that produced it.
* Author or creator.
* Other metadata such as the results of the security scan, as well as the version of the scanner and its configuration.

These results need to be cryptographically signed so that a third party can trust that the creator of the artifact didn't forge them.

#### Examples:

* Docker images:
   * Gitaly, kas, main Rails app, Runner, Shell, etc
* Helm charts with default values
* CI artifacts
   * Source code scans, Container image scans, code coverage, unit test reports, license compliance checks
* Terraform plan
* Archives of directories
   * Kpt package

### Configuration

A configuration is an immutable set of key value pairs denoting the rules or settings for a particular application. The configuration can be identified by a unique hash and is part of a deployment. It's populated by data coming from:

* Application and environment specific configuration set up either by the Application Operator or the [the Platform Engineer](https://about.gitlab.com/handbook/marketing/strategic-marketing/roles-personas/#priyanka-platform-engineer).
* Application specific configuration set up by the [Application Operator](https://about.gitlab.com/handbook/product/personas/#allison-application-ops).
* Processes or the platform within GitLab (e.g. the user ID running a pipeline).
* Target infrastructures set up by the Platform Engineer.

#### Example

In the case of gitlab.com, configuration is stored in:

* Config files in [repo](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com).
* In the CI definition in [the repo](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com) there is environment specific configuration.
* Helm charts with its defaults is part of the release.

Defaults baked into the application is part of the release * we don’t consider these configurations.


### Deployment

A deployment is an application instance configured for a given environment. There can be one or more deployments within each environment. 

**Deploying** is the potentially long-running act of updating an environment with a new version of the application and the relevant configuration.


### Environment

An environment is a logical concept that describes part of its target infrastructure with a rich set of metadata. It

* describes the history of deployments,
* contains any deployed configuration,
* can deploy to its target infrastructure,

Environments can be of different types:

* in terms of functionality
   * production
   * non-production
* in terms of lifespan
   * long-living
   * ephemeral

Every environment has a single target infrastructure, a target infrastructure might have many environments. Thus multi-region setups require multiple environments.

You can have multiple instances of a given type (for example by region, provider, applications served). It’s useful to query the system by environment type, to be able to build dashboards around environment types.

Environments might be dynamically created during the pipeline execution, they might be templated.

#### Example

GitLab environments. Examples: gprd, gstg, pre

   * These are provisioned in Terraform
   * The deployed version is stored in the repository together with the configuration of that deployment ([example](https://gitlab.com/gitlab-com/gl-infra/k8s-workloads/gitlab-com/-/merge_requests/1099 ))
     * The version of the app deployed is part of the environment configuration
   * A log of deployments: a deployment is a CI job that matches up to an environment

### Rollout

A rollout is the action of putting an already deployed application in front of its users. This might happen in many ways:

- Change the infrastructure, for example by configuring a load balancer.
- Change the application code flow using a feature flag. This does not require a new deployment.

The rollout step might happen together with a deployment or as a separate action.

A rollout might take many forms. For example:

- blue-green
- canary
- user or group based
- random percentile-based
- region based
- device based

As noted above, there are two big approaches to rollout. The following table summarizes provides a comparison:

| Feature flag | Network level |
| -----------  | --------------  |
| Changes the executed code path within a single deployment. | Changes the network traffic routing between deployments. |
| Typically manual rollout | Could be automatised based on error rates and metrics |
| A property of the environment. All the running deployments receive the same feature flag values. | A shared property of environment and deployment. Every deployment has its own rollout state. |

While we have two different approaches to rollout. We decided to refer to them as rollout for now. We will update the glossary if needed.

### Secrets

Secrets are part of the Configuration, typically key-value pairs that allow privileged or secure access between two or more components. Their management requires special attention, but it's outside the scope of this discussion. [See the Secrets Management category direction](https://about.gitlab.com/direction/verify/secrets_management/) on our approach to secrets.


### Target infrastructure

A target infrastructure in an immutable entity that contains one or more environment, along with applications that run in that environment. A target infrastructure typically runs multiple applications, but is not application specific. 

From GitLab's perspective, target infrastructures are considered external systems. The target infrastructure is responsible for the authenticating of GitLab with the infrastructure. Authorization is managed based on the respective best practices of the given infrastructure (e.g. IAM, RBAC). Accesses may be restricted to certain jobs or users and may be logged for compliance and be programmable. For example, retrieving and returning a JWT token. 
