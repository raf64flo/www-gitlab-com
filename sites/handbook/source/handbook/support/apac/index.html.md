---
layout: handbook-page-toc
title: Support Team APAC
description: Support Team APAC home page
---

## Welcome to Support Team APAC's Handbook page!

This page documents items specific to Support Team APAC which we have not yet
found a home for in the wider Support Team Handbook.

The intent is to enable APAC Support team members to contribute to Results for
APAC-specific iniatitives, policies, processes and workflows by prioritizing:

1. Transparency, through being [handbook first](https://about.gitlab.com/handbook/handbook-usage/#why-handbook-first)
   and providing a single source of truth for APAC-specific.
1. Iteration, through providing a safe space where APAC Support team members can
   introduce or update APAC-specific policies, workflows and processes without
   creating confusion and uncertainty for the wider Support Team.

Where appropriate, we should always look to move content from this page into
other pages of the wider Support Team Handbook. For an example of how this can
be done, see the [Considerations in APAC section](https://about.gitlab.com/handbook/support/on-call/#considerations-in-apac)
of the GitLab Support On-Call Guide Handbook page.

## General policies

### Support Team APAC is One Team

* APAC Support Readiness department members are part of Support Team APAC too.
* Avoid calling individual manager's direct reports group "my team" or "your team".

### Support engineers should be able to work across all GitLab product platforms

* Support engineers should be willing and able to work on problems for all
  platforms supported by GitLab: SaaS, Dedicated and Self-Managed.

### Support engineers should spend time on work other than L&R work

* ???

## FY24-Q1 Motto: Solve tickets faster

We are evaluating turning this into our north star for FY24. Please leave any
thoughts or feedback in the [discussion issue](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/4921).

## Daily Bot in the #support_licensing-subscription slack channel

There is a daily bot that follows the SGG bot format and tags all APAC Support 
engineers who have a Focus `name: License and Renewals` listed in their [Support Team Page](https://gitlab.com/gitlab-com/support/team/-/blob/master/data/support-team.yaml) entry. 

Example Support Team Bot post: 

> _Support Team Bot
Morning APAC @name1 @name2 @name3 @name4 @name5 @name6 @name7. Today we have:  7 working, 1 on PTO:_
> 
> _The following people have scheduled PTO:_
> _* Wednesday: @name8_

Support engineers update this post's thread daily to share with each other when
they are covering the queue, so that team members are confident that there are eyes
on the queue when they complete their scheduled time to action these tickets. There
is no strict roster, team members opt-in and share their availability to achieve 
coverage. 

## Holiday Coverage Planning

We are mindful of [holidays](https://about.gitlab.com/handbook/support/support-time-off.html#holiday-time-off-ticket-management) that impact large parts of the team. The following are official holidays for mostly APAC team members, which we plan coverage for outside of global practices: 

* Impact to both APAC Groups 1 and 2: 
    * Australia Day/India Republic Day (26 January)

* Impact mostly to APAC Group 1 
    * Easter Friday 
    * Easter Monday
    * Anzac Day (25-April)

To refer to past planning issues, see issues linked to the [[APAC] Holiday Coverage Planning Issues epic](https://gitlab.com/groups/gitlab-com/support/-/epics/252).


## Regular Sync Sessions

<table>
  <thead>
    <tr>
      <th>Weekly</th>
      <th>Bi-Weekly/Fortnightly</th>
      <th>Monthly/Irregular</th>
   </tr>
  </thead>
  <tbody>
    <tr>
      <td>
        <ul>
          <li>APAC Crush</li>
          <li>EMEA/APAC Crush</li>
          <li>Support Team Call</li>
          <li>L&R Meeting</li>
        </ul>
      </td>
      <td>
        <ul>
          <li>Anton Sm's Office Hours</li>
          <li>Julian's Kubernetes Help Sessions</li>
        </ul>
      </td>
      <td>
        <ul>
          <li>APAC/AMER Crush</li>
          <li>Release Review Party</li>
          <li>APAC Office Hours</li>
          <li>Social Call</li>
          <li>APAC Book Club - [Tribal Leadership](https://gitlab.com/gitlab-com/support/support-team-meta/-/issues/4854) <br>weekly between 2023-02-16 to 2023-04-06</li>
         </ul>
      </td>
    </tr>
  </tbody>
</table>


* ??? 

