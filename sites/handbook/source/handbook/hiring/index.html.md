---
layout: handbook-page-toc
title: "Hiring"
description: "Landing page for many of the handbook pages the talent acquisition team at GitLab uses."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

{::options parse_block_html="true" /}

## Executive Summary
Over the next 3 years, GitLab has a [strategy](https://about.gitlab.com/company/strategy/#three-year-strategy) to ensure GitLab is the default when customers buy DevSecOps software, mature our platform, and grow GitLab careers and beyond. 

As a Talent Acquisition team, we are responsible for enabling the hiring of talent that is essential to achieve this strategy. This page outlines our vision, mission, and a fiscal year strategic roadmap to position GitLab for success. 

## Introduction 
At the core of our team’s vision, mission, and strategy is our ability to impact GitLab’s overarching [mission](https://about.gitlab.com/company/mission/): to make it so that **everyone can contribute**. When **everyone can contribute**, users become contributors and we greatly increase the rate of innovation. 

As a Talent Acquisition team, we have an outsized impact on GitLab’s ability to make this mission a reality, by connecting top talent to profound careers from wherever they are in a truly distributed, remote workforce. 

### Talent Acquisition Vision Statement
To create globally inclusive access to opportunities so that **everyone can contribute**.

### Talent Acquisition Mission Statement
It is the Talent Acquisition Team’s mission to predictably build distributed, representative teams that enable our vision of creating globally inclusive access to opportunities so that **everyone can contribute**. 

### Our Guiding Principles
As we set out over the next decade to achieve this vision, we will continue to rely on core guiding principles to define how we build toward the future. 
  1. **Experience**: Stakeholder experience is central to all that we do. We are not purely a service provider but a partner and advisor in creating a best-in-class experience while building GitLab as a company. The Talent Acquisition team looks after building an experience for our customers, stakeholders, and partners that stands true to our [CREDIT](https://about.gitlab.com/handbook/values/) values and provides a level of care we are proud of - no matter the outcome. 
  1. **Inclusivity**: Applying best practices in the craft of Talent Acquisition has an outsized impact on our ability to build a representative workforce here at GitLab. By embedding inclusivity as a guiding principle, we design fair and equitable processes into the fabric of all that we do, rather than retroactively adding parameters to solve systemic challenges in the societies we exist in   
  1. **Predictability**: Our ability to have the right people, in the right jobs, at the right time is imperative to our ability to execute our commitments and plans as an organization. In order to achieve that, our team must design with an eye for accuracy and forecastability in any program, process or experience we enable. 


## Hiring pages
- [Meeting Cadence](/handbook/hiring/meetings/)
- [Interviewer Prep Requirements](/handbook/hiring/interviewing/)
- [Conducting a GitLab Interview](/handbook/hiring/conducting-a-gitlab-interview/)
- [Diversity, Inclusion & Belonging  Talent Acquisition Initiatives](/company/culture/inclusion/talent-acquisition-initiatives/)
- [Greenhouse](/handbook/hiring/greenhouse/)
- [Prelude](/handbook/hiring/prelude)
- [Overview of Job Families](/handbook/hiring/job-families)
- [People Technology & Insights](/handbook/hiring/talent-acquisition-framework/talent-acquisition-operations-insights/)
- [Principles](/handbook/hiring/principles/)
- [Talent Acquisition Alignment](/handbook/hiring/recruiting-alignment/)
- [Talent Acquisition Privacy Policy](/handbook/hiring/recruitment-privacy-policy/)
- [Talent Acquisition Process Framework](/handbook/hiring/talent-acquisition-framework/)
- [Referral Operations](/handbook/hiring/referral-operations/)
- [Referral Process](/handbook/hiring/referral-process/)
- [Resource Guide](/handbook/hiring/guide/)
- [Sourcing](/handbook/hiring/sourcing/)

Potential applicants should refer to the [jobs FAQ page](/handbook/hiring/candidate/faq/).

## Related to hiring

- [Background checks](/handbook/people-policies/#background-checks)
- [Benefits](/handbook/total-rewards/benefits/)
- [Compensation](/handbook/total-rewards/compensation/)
- [Contracts](https://internal-handbook.gitlab.io/handbook/people-group/people-operations/people-connect/employment_contracts/)
- [GitLab talent ambassador](/handbook/hiring/gitlab-ambassadors/)
- [Onboarding](/handbook/people-group/general-onboarding)
- [Stock options](/handbook/stock-options)
- [Visas](/handbook/people-group/visas/)
