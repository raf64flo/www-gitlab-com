---
layout: handbook-page-toc
title: 6sense
description: 6sense Overview
twitter_image: /images/tweets/handbook-marketing.png
twitter_site: '@gitlab'
twitter_creator: '@gitlab'
---
## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

# <i class="fab fa-gitlab fa-fw" style="color:rgb(252,109,38); font-size:.85em" aria-hidden="true"></i> 

This page is under construction while we work to migrate from Demandbase to 6sense. Please feel free to improve this page by opening an MR! 
{: .alert .alert-warning}


## Overview

[6sense](https://6sense.com/) is a an [Account Based Marketing](https://about.gitlab.com/handbook/marketing/account-based-marketing/) platform that uses a predictive model to identify the right customers at the ideal time.  

## Implementation

Currently being implemented in [this epic](https://gitlab.com/groups/gitlab-com/marketing/-/epics/3691)

## Integrations

6sense is integrated with Salesforce and Marketo


