---
layout: handbook-page-toc
title: "Global Channel Marketing"
description: "Global Channel supports global channel sales objectives and providing support to the GitLab channel partner community and customers."
---

## On this page
{:.no_toc .hidden-md .hidden-lg}

- TOC
{:toc .hidden-md .hidden-lg}

## This page is intended for the following audiences

GitLab global and regional channel and alliances sales teams, global and local field sales teams, regional [field marketing](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing--channel-marketing) teams, channel account managers, the global alliances team and product marketing team.

## Our Team
Global Channel Marketing team is a team of seasoned professionals supporting global channel sales objectives and providing support to the GitLab channel partner community and customers.  This small but mighty team is responsible for the development of channel marketing campaigns that leverage GitLab’s Go to Market Campaigns and value plays.  We work with partners to help them understand what the campaigns are, how to use them to help drive partner sourced opportunities, how to leverage upcoming GitLab webinars/webcasts to drive conversion and how to generate trials for GitLab that they can nurture with prospects.  Channel partners are ultimately responsible for the execution of these campaigns.  

The Global Channel marketing team provides support to partners by creating different campaign assets into leverageable go-to-market programs called Instant Campaigns that partners can easily pick up and run with their customer or prospect lists.  GitLab channel marketing is also responsible for the development, rollout and management of trial enablement programs by which partners can generate trials of GitLab directly from their website, and passing or providing trial leads generated from GitLab’s own website to be worked and converted by partners.  

Channel marketing also enables channel go to market efforts through MDF funding and management, by identifying upcoming field marketing events and activities that channel partners can participate in, and by identifying opportunities for channel partners to participate with Alliances partners and GitLab on joint go to market activities.

We partner with regional Channel Account Managers and Field Marketing regional teams to work closely with channel partners and GitLab Alliances for inclusion in their regional marketing plans.  

As a service bureau to a wide variety of teams, we have support functions that are both in and out of scope at this time.  We have ideated on a list of potential future service capabilities that, as this team is able to add resources and our business plan requires it, we will add to our list of service offerings.  To recommend that we add a service offering to this growing list of potential offerings, please create an issue.

## Current Service Offerings

*.  For an in-depth look at the programs and tools we offer our partners, see our [marketing menu of services](https://about.gitlab.com/handbook/marketing/channel-marketing/channel-marketing-epics/Channel-Marketing-Service-Offerings/) page. 
*   Administering [Marketing Development Fund](https://about.gitlab.com/handbook/marketing/channel-marketing/#requesting-mdf-funds) (MDF) to provide financial reimbursement for APPROVED partner marketing campaigns and events.  Requests must support GitLab GTM initiatives and adhere to the MDF Request Process detailed below. MDF reimbursement will be at 50% of the total campaign request. There are three approval levels and each level has 2 business days to review and approve/decline the MDF proposal. 
*   Turnkey, integrated [instant marketing campaigns](https://about.gitlab.com/handbook/marketing/channel-marketing/#What-is-a-Partner-Instant-Marketing-Campaign) provide the collateral materials partners need to run a demand-generation campaign.
    *   For additional information on localization, please visit the [Localization](https://about.gitlab.com/handbook/marketing/localization/#priority-countries) Handbook page.
*   Global [external virtual events, workshops](https://about.gitlab.com/handbook/marketing/virtual-events/external-virtual-events/), and third party events and webcasts  leveraging Alliance partner “better together” messaging and joint GTM solution customer value propositions.
*   In partnership with [Channel Programs Operations](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops/): 
    *   Develop marketing campaign that targets partner Solution Architects to become advocates of GitLab by delivering GitLab solution overviews, demos and proof of concepts to uncover and convert sales opportunities
    *   Enable channel partner marketers to participate in [FY22 global GTM](https://about.gitlab.com/handbook/marketing/plan-fy22/) motions in partnership with the [GTM team](https://about.gitlab.com/handbook/marketing/plan-fy22/#core-teams).  
        *   Build and deliver a Bill of Materials of channel-friendly and consumable versions of GitLab campaigns and assets 
*   Provide review and approval for local/regional [field marketing](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing--channel-marketing) plans with partners
*   Provide review and approval of Channel Account Manager (CAM) marketing plans.
*   Build a repeatable, predictable, sustainable and desirable model enabling partners to receive, accept, nurture and close GitLab leads leveraging the GitLab Partner Portal and Vartopia Deal Registration system.  (future link to documentation of the process)
*   Enable channel partners to add “[GitLab free trial](https://about.gitlab.com/free-trial/)” functionality to their websites with marketing tools and lead passing to allow partners to effectively generate and nurture their own leads.  This will serve as partner primary CTA within demand generation efforts. 
*   For Select Partners, participate in partner QBRs and provide input and ideation on successful go to market campaigns and initiatives to help partners drive more business with GitLab. 
*   On a monthly basis, provide marketing content and input to the Global Channel Operations team for use in the global channel communication/newsletter 
*   On a quarterly basis, host global marketing webcasts to the partner community.
*   Support GitLab [Analyst Relations](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/analyst-relations/) requests for channel program input on MQs, WAVEs, market guides, as needed and with third party channel analysts.  
    *   Support requests for inclusion in third party publications
*   Support the web team in maintaining GitLab’s marketing website [partner pages](https://about.gitlab.com/partners/)
*   Working with Sales Ops and Marketing Ops improve attribution and reporting of [partner sourced leads (CQLs), and partner sourced opportunities](/handbook/sales/field-operations/channel-operations/). 

## Quarterly Planning: Field and Channel

As GitLab evolves, the growth of the channel becomes a critical component to our overall strategy of growth and new logo capture.  With established teams in place, it's time to take our engagement up a level and as such, review how we partner together for proactive, and efficient engagement that results in high quality demand generation plans and delivering actionable leads for our sales teams.
In order for our collective plans to work, it's first important to verbalize the roles, responsibilities and focus areas for each functional team.

13 weeks prior to the start of the following quarter, planning for that quarter will commence.  FMM's and CAM's are expected to partner together and setup regular, consistent planning calls, preferrably bi-weekly. The focus of each cadence call will change as the plan formulates.  Specifically: 
*   ### Weeks 1-2 (11-13 weeks prior to the future quarter start):
Develop partner campaign objectives and set goals (specifically NFO SAO goals).  Set up recurring biweekly cadence with the individual partner, CAM and FMM to keep planning and campaign action items on track.
*   ### Weeks 3 and 4 (9-11 weeks prior to future quarter start): 
Based on campaign goals, identify the regions where pipeline needs are the greatest.  Architect campaigns to support that/those regions.  What are the campaign elements/tactics needed to create interest?
*   ### Week 5 and 6 (7-9 weeks prior to future quarter start): 
Agree to campaign tactics and persona targets, develop budget request and expected outcomes in the form of MQL's and SAO's.
*   ### Week 7 and 8 (5-7 weeks prior to future quarter start): 
Formalize/finalize the tactics, define action items and workback plan.  Identify any speaker needs.  Final agreement on MQL and SAO targets.  Complete FY23 Partner Planning Template and submit back to FMM and CAM.
*   ### Week 9 (4 weeks prior to future quarter start): 
FMM and CAM submit holistic budget request to Channel Marketing for MDF approval.

## Team Roles and Responsibilities
*   ## Channel Account Managers 
Own the strategy, relationship and ongoing, consistent engagement with the Select partners in their territory.  The channel account manager is responsible for the holistic success and health of the partner relationship. 

*   ## GEO Channel Director
 Sets the strategy for the region, manages the Channel Account Managers and approves the criteria for Select and Open partner selection in the region.
*   ## Field Marketing Managers
In partnership with field sales, drive MQL's to convert to SAO's.  The field marketing managers are responsible for articulating which areas of the business require the most attention and where we need channel partner engagement to drive demand generation plans.
*   ## GEO Field Marketing Director
Develops the strategy and conversion targets for the team in partnership with field sales leadership.
*   ## Partner Program Marketing Managers
Develops scalable marketing programs to support Partners. 
*   ## Channel Marketing Manager
Responsible for supporting CAMS in specific geographies, by creating demand generation marketing plans with Focus Partners. In charge of planning MDF activites and improving Partner's participating in various Channel programs.

Once a plan for the quarter has been established it is imperative that the channel account manager and the field marketing manager stay in close, consistent alignment about plan execution.  The strategy is driven by the channel account manager, while the field marketing manager is responsible for executing on that strategy in partnership with various support teams, including but not limited to the channel account managers.

[MDF](https://about.gitlab.com/handbook/marketing/brand-and-product-marketing/product-and-solution-marketing/analyst-relations/channel-marketing/#requesting-mdf-funds) 
CAN and SHOULD be leveraged as much as possible when the partner is involved in an activity.  Further information about how to initiate and MDF request can be found here.

## Tracking Success:
GitLab Marketing receives 'credit' for opportunities that are partner sourced so long as the oportunity has a BATP (Bizable Touchpoint) from the campaign.
Check out the [field marketing handbook page](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing-initiated-and-funded-campaigns)
that details the FMM process where CAMs can read more about how to complete [the lead gen issue.](https://gitlab.com/gitlab-com/marketing/field-marketing/-.issues/new?issuable_template=Channel_LeadGen_Req)

## Requesting MDF funds
* Approved Select Partners will submit [MDF proposals through the Partner Portal](https://partners.gitlab.com/English/Partner/SFDC/MDF2/Request/Create), and GitLab approvers will be notified of the request via email.
    +  The MDF proposal will go through 3 levels of approvals 
       + Level 1 approval - Channel Marketing Manager (CMMs)
       + Level 2 approval - Samara Souza, Sr Partner Program Manager
    +  Once the MDF request has been either approved or declined the partner will be notified: 
          + If declined, the Channel Marketing DRI will reach out to the partner and let them know. We will copy the CAM on the notificiation
          + If approved, the MDF Operation's team (Samara Souza or Teri Stone) will [open an epic](/handbook/marketing/channel-marketing/channel-marketing-epics/) and correlating sub-issues defined therein. 
    + Once Epic has been created, channel marketing manager will create the sub-issues listed in the epic. Includes (links are to templates):
       + Campaign Creation (Assign to Verticurl and add in ~verticurl label)
         + List Upload
          + MDF POP 
             + For POP issues add labels:
             + `MDF-POP` and use the scoped lables below to work through the approval process:
             + `MDF::POP Pending` issues created but POP has not yet been received
             + `MDF::POP Approval Needed` once POP is received change this status to have Samara Souza approve the POP documents submitted
             + `MDF:: POP Approved` POP has been reveiwed and is approved, list can be uloaded into List upload issue, Zip requistion can be created
             + `MDF:: POP Completed` Once all of the steps above are completed, change to this status and close the MDF POP issue
         + Make sure to add issues to the epic!
    + Once all sub-issues are completed, close out the epic.
        

### Demand Generation activities eligible for MDF
*  **Customer Case Study** - Development of a print or digital case study that details an end-user customer’s positive experience with GitLab. Must highlight how a GitLab product or service solved a customer’s business problem, benefit received by customer, and customer’s endorsement of GitLab.
*  **Telesales/Appointment Setting** - Outbound lead generation telemarketing campaigns that target end-user prospects /customers that promote GitLab or GitLab Alliance joint products or solutions.
*  **Digital & Print Marketing** - Creation of broad reach promotional and brand marketing content for GitLab products or solutions in digital or print targeted at end-user customers or prospects. Must contain a customer call to action.
*  **Paid Search Events/Webinars** (includes workshops, roundtables) - End-user customer or partner event executed by a Partner to generate net new business and / or build pipeline. Must be focused on GitLab and / or joint solutions with Alliance Partners.
* **Tradeshows/Conference Sponsorships** - Partner on-site sponsorship of a Third-party event organized by an industry set, or other GitLab Partner, where a sponsorship contract / package is purchased by Partner and GitLab products, solutions, & brand are promoted.

These activities are captured as `Campaign Type Details` on the campaign level in Salesforce.com for `Partner - MDF` [Campaign Type](/handbook/marketing/marketing-operations/campaigns-and-programs/#campaign-type--progression-status) only.

### Submitting Proof of Performance (POP) and Invoice for MDF 

* Once the MDF campaign is completed, the partner has 30 days to submit their [Proof of Performance (POP)](https://partners.gitlab.com/English/Partner/SFDC/MDF2/Manage) and Invoice for reimbursement.
    + Click on the MDF activity listed
    + Go to attachements and upload POP and Invoice
* Once a POP is completed, the Partner Program Manager will create an issue [`channel_mdf_pop`](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new#channel_mdf_pop) and complete Step 1 
   + MDF POP labels need to be created in order to track POP process. Labels as listed above.
* Once Step 1 is completed, Samara Souza or supporting team member will open a Zip requisition for reimbursement payment to the partner

## Requesting Swag for Channel Events and Awards
CAMs can now order swag for approved co-marketing events or other approved awards through Channel Marketing’s partner swag portal.  Please allow 10-15 business days for the approval and processing of your request plus additional shipping time. The event start date cannot be within 3 weeks of the request as items may not arrive in time. Please allow plenty of time for the Swag to be delivered.

**Please note we have a new vendor, and the new portal information will be available shortly. In the meantime please follow the following steps to get your swag ordered**
1. Reach out to your Channel Marketing Managers
2. Have the following information ready: date of the envent, contact information and shipping address
3. CMMs will coordinate with the CAM/Parnter directly to get the items shipped
 
## What is a Partner Instant Marketing Campaign?

With GitLab Instant Marketing campaign assets, GitLab’s Select and Open partners have access to marketing resources to help them quickly, and easily generate qualified prospects for their sales teams. We’ve developed turnkey, integrated partner campaigns-in-a-box, which we refer to as “Partner Instant Marketing” campaigns, so partners can simply and easily co-brand assets and execute!

These assets leverage GitLab go-to-market motions, such as DevSecOps and Automated Software Delivery, but are modified and expanded (in some cases) for partner use. Campaign assets include: nurture emails, landing pages, short videos, mini-books, social media and paid ads, quick sales guides and sales call scripts. All assets can be found on theGitLab partner portal. 

Our modular campaign is designed to be a flexible, self-service model, so partners can download an entire campaign, or a couple of assets, and the materials are customizable and co-brandable. These campaigns are also available in 6 languages: English, French, German, Japanese, Korean and Spanish. 

Here’s a look at our current campaigns available to partners by logging into the [Marketing Demand Generation section](https://partners.gitlab.com/prm/English/c/marketing-demand-gen) of the partner portal:

- GitLab Free Trials 
- GitLab with DevOps 
- GitLab with DevSecOps
- GitLab Ultimate Upgrade 
- GitLab Automated Software Delivery 

## Partner-Initiated GitLab Free Trial Lead Gen Program

Get all the assets you need to host a GitLab Ultimate SaaS or Self-Managed free trial custom link on your own website and run an end-to-end multi-touch campaign, offering customers and prospects the opportunity to try before they buy. We provide two unique tracking links that direct your prospects and customers to the GitLab 30-day Self-Managed or SaaS free trial registration forms. Free Trial lead requests come in and are tagged with your partner ID and then routed automatically into the partner portal deal registration prospect section. Partners can access and manage their GitLab leads in the same interface as their [deal registration](https://about.gitlab.com/handbook/resellers/#the-deal-registration-program-overview).

We currently offer the Self-Managed form in English, Spanish, German, French, Korean and Japanese and the custom SaaS free trial form in English.

*   [Reference the Free Trial Introduction Guide](https://partners.gitlab.com/prm/English/s/assets?id=433002&q=free%20trial) 
*   [How to grow business using Microsites, Free Trial and Vartopia lead passing Video](https://partners.gitlab.com/prm/English/s/assets?id=429634&q=vartopia) 
*   [GitLab Lead Management Demo](https://partners.gitlab.com/prm/English/s/assets?id=443125&q=vartopia) 
*    [GitLab Lead Management Prospect Guide](https://prod.impartner.live/en/s/assets?id=430453&q=prospect)

## How to Sign up for the GitLab Free Trial Program 

- Reach out to the GitLab Global Channel Marketing team via email at partner-marketing@gitlab.com to get your custom free trial link(s) and assign your your Partner Prospect Admin. This role will need to be assigned before you [test](https://partners.gitlab.com/prm/English/s/assets?id=439667&q=prospect) and use your free trial link. 
   
   - Please note if you are using the SaaS free trial form you must add the following consent on your website and any marketing materials you are using that point your customers to the SaaS free trial form:

      _I agree that GitLab may share the data in this form with the GitLab partner that referred me to this landing page and GitLab's partner may contact me by email or telephone about relevant products, services and events. You may opt-out at any time by unsubscribing in future emails or contacting the GitLab partner directly._


- Next, test your custom URL to ensure you are recieving the leads inside the partner portal deal registration prospect section. 

   - Learn more about where to find your leads by watching this [video](https://player.vimeo.com/video/807924930). 

 - Use our [ready made assets](https://partners.gitlab.com/prm/English/c/marketing-free-trial#:~:text=Step%203%3A%20Get%20the%20word%20out) to create co-branded display ads, social posts, and emails to invite prospects to the trial. Link to your custom free trial URL to drive traffic to the Free Trial landing page. 
 
 -  Once a user registers for a free trial on the landing page, they receive an auto-generated email from GitLab with their license key. Your next step: nurture and convert those leads! The links below provide you with the copy and supporting assets that you need to run your own email campaign to nurture your leads over the next 30-days on their buyer journey, from evaluation to purchase.
 
 You can download the full kit of free trial assets [here](https://partners.gitlab.com/prm/English/s/assets?id=439714&q=free%20trial%20kit).

## GitLab Hosted Partner Microsite Program

Content syndication can be a great way for organizations to get information and messaging onto partnersʼ websites. But often, this content isnʼt optimized for search engines, plus it can become outdated and inaccurate unless partners maintain it, and it offers only limited metric tracking. To help our partners have access to the latest content available, we created a [demand generation Partner Microsite Program](https://partners.gitlab.com/prm/English/s/assets?collectionId=49429&id=429634_View). With a little information and commitment from you, weʼll build and maintain a co-branded microsite with compelling information for your customers. We offer the microsite in English, Spanish, French, German, Japanese and Korean.

Check out a [sample microsite](https://learn.gitlab.com/updated-partner-engl).

## How to Sign up for the GitLab Hosted Partner Microsite Program

- Provide us with your company logo, contact information for a designated person to receive and take action on leads and a value statement for your company (value statement optional).
- GitLab generates a co-branded microsite with content that is consistently refreshed with the latest GitLab solution-level messaging. The microsite will include a customer call-to-action for a 30-day Free Trial. Weʼll provide you with a unique URL to include in your nurture efforts. Note, the call-to-action for this campaign is for customers to take a 30 day free trial. We currently offer the Self-Managed free trial on our English, Spanish, Korean, Japanese, French and German sites and the SaaS free trial on our English sites.  Note, you can add the 30-day free trial functionality to your own website by accessing [our Free Trial campaign](https://about.gitlab.com/handbook/marketing/channel-marketing/#partner-initiated-gitlab-free-trial-lead-gen-program).
- You commit to running at least one full nurture effort (including at least three emails **or** a three-month Google ad campaign), pulling a target lead list with a minimum of 50 customers from your own database, taking follow-up action via either telemarketing or direct sales contact on all leads routed to your organization, tracking the results of the leads through your own Salesforce system, and providing GitLab with a report three months after the completion of the campaign. Note: You can create the content for your nurture emails or Google ad campaign, or you can leverage materials in GitLab's [Instant Marketing Campaigns](https://about.gitlab.com/handbook/marketing/channel-marketing/#What-is-a-Partner-Instant-Marketing-Campaign).
- You can schedule custom reporting to recieve leads that come in from your nurture emails and marketing efforts. 
- GitLab will provide you with metrics reports on a bi-weekly basis.
- GitLab will manage any changes or updates to the microsite and notify you of changes.

## Future/Potential Service Offerings

*   Branding, awareness and promotion of GitLab channel partner badges
*   Partner marketing concierge services (dependent on funding/resources)
*   Joint demand generation campaigns with partners
*   Partner Sales Engineer/Solution Architect marketing program (to become advocates of GitLab)

## Channel Partner Program Participation Overview

Channel Partners can enroll in multiple Partner Programs including the Microsite, Free Trial and UserEvidence offerings. We keep track of the program participation by updating the Salesforce Activity, which can be reviewed in a [SFDC Report](https://gitlab.my.salesforce.com/00O8X00000963dq). 

Anytime a partner registers to a new program, the Channel Partner Program DRI must complete the following steps to ensure we can accurately report on the program participation:

[Video instructions >](https://youtu.be/ULBMqQh03n8)

1. Identify the Partner Account of the Channel Partner Program
    
    1a. Ensure the Account Detail - `Account Record Type` = `Partner`
    
    1b. Ensure the GitLab Partner Program Info - `Partner Type` = `Channel`

2. On the Account page layout, go to Open Activities, select a `New Event`

3. In the New Event, add the following:

   3a. Input the Subject: 
      - `Microsite Program - Partner Site`
      - `Microsite Program - Sales Site` 
      - `Free Trial Program - Self Managed` 
      - `Free Trial Program - SaaS` 
   
   3b. Select the `Type` to `Marketing`

   3c. Unselect `Reminder`
   
   3d. Save

Note, the subject needs to be an exact match of what's displayed above, otherwise, it will not show up in the SFDC report.

## Beyond our Team’s Scope

*   Individual partner support, planning, ideation and execution including but not limited to joint demand generation campaigns.  Please work closely with your region’s [field marketing](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing--channel-marketing) team and reference linked Handbook pages for self support and field support of these programs.
*   Custom campaigns: Channel Marketing does not currently have the resources to support individual partner campaigns and events. Please encourage your partners to leverage one of our turnkey, and integrated [Instant Marketing Campaigns](https://about.gitlab.com/handbook/marketing/channel-marketing/instant-campaigns/) available on the [Partner Portal](https://partners.gitlab.com/English/).  For unique virtual and live events, the CAM should engage with their [field marketing](https://about.gitlab.com/handbook/marketing/field-marketing/#field-marketing--channel-marketing) counterpart to leverage field budget and marketing support for joint events.  The Field Marketing Manager will engage with their sales teams and support the CAM and partner with development, execution, and lead follow up for  the event.
*   Unique Partner Asset Creation: Channel Marketing is unable to work directly with partners to customize their marketing assets. 
*   Joint demand generation planning and execution: **The GitLab CAM is the primary point of contact to enable their partners run a successful campaign or event.**  
*   Event speakers: The Channel Marketing team does not have the resources to help locate GitLab speakers for partner events.
*   Partner Blogs: The Channel Marketing team does not have the resources to craft unique content to support a partner blog or content request.
*   Press releases: The Channel Marketing team does not have the responsibility to edit and approve partner press releases.  Please create a [Channel or Tech Partner Announcement issue](https://gitlab.com/gitlab-com/marketing/corporate_marketing/corporate-marketing/-/issues/new?issuable_template=channel-partner-announcement-request) for coordination with the Corporate Communications/PR team
*   Sponsored social media posts: partners looking to GitLab to promote partner activities would not route those requests through Channel Marketing.  Instead, CAM’s obtain the partner’s social media channels and [create an issue](https://about.gitlab.com/handbook/marketing/integrated-marketing/digital-strategy/social-marketing/admin/#open-a-new-issue-to-request-social-coverage) for the Social Media team.  This request should include the partner’s social media information, detailed description of what we are being asked to promote and target dates.  
*   Event attendance: The Channel Marketing team does not own a database of contacts by which to drive attendance to partner specific events nor do we possess the resources to support such requests.
*   Partner training:**<span style="text-decoration:underline;"> [Partner training](https://about.gitlab.com/handbook/resellers/training/)</span>** is managed and supported by the [Channel Partner Training, Certifications, and Enablement team](https://about.gitlab.com/handbook/resellers/training/).
*   Partner portal management: [Partner portal administration](https://about.gitlab.com/handbook/sales/channel/channel-programs-ops/#partner-portal-administration) is managed by GitLab Channel Program Operations.

## Service Level Agreement

When requesting support from our team, we commit to responding to your [issue](https://gitlab.com/gitlab-com/marketing/partner-marketing/-/issues/new?issuable_template=channel_partner_request) within 2 business days (when request is easily understood) or a Zoom meeting invite to understand the request, scope, timeline, goals and expectations.  

## Alliances Partner Marketing Roles and Responsibilities
**Alliances Marketing Team**

The role of alliances partner marketing manager is to own and develop the demand generation plan for and with our alliances partners based on the account strategy developed by Alliances Director and in alignment with company SAO goals for the segment and region.

**Account assignment/alignment as of Sept 2022:** 

* **Gabby Chorny**: IBM and RedHat aligned to Brett Egloff and VMware aligned to Vick Kelkar.

  Gabby will also provide support to the **ISV partners** in the form of up to 5 solution briefs per quarter at the direction/priority of Mike LeBeau, Alliances manager. 

  *As of Sept 2022 Gabby will commit to providing 5 solution briefs for key ISV partners.  Future support will require discussion, planning and budget.  See **Coleen Greco** for additional details.

While each account does have it’s own unique go to market motion the **following is a list of what the Alliance marketing manager is responsible for:** 

* **Develop and maintain the alliance partner marketing relationships.**
   * Our goal is to work with the assigned marketing manager but expand our relationships far and wide within the marketing org to support our strategic objectives. 
* **Based on the Alliance Director’s strategy, the Alliance marketing manager will develop a long-term, strategic marketing plan that reflects the goals and objectives of the partnership.** 
   * Alliances Directors must ensure the marketing managers have consistent visibility into pipeline goals by segment and by region including average deal size, sales cycle and etc. 
* **Develop the plan and tactics that support demand generation activities and in alignment with the marketing strategy.** 
   * Demand generation tactics include but are not limited to: solution briefs, ebooks, blogs, social, digital, webinars and events.
* **Campaign management**
   * Marketing managers are responsible for the components of the campaign including the set up, testing and execution of: 
      * Campaign nurture tracks (unless executing a field mktg event)
         * Accurate lead flow
      * Marketo campaigns (unless executing a field mktg event)
      * Weekly reporting (Sales Accepted Opptys, New First Order)  
* **Content editing**
* **Content creation with an external party** 
    * Note: marketing managers do not author the content but coordinate with outside vendors, and subject matter experts to execute and ensure legal and brand compliance.
* **Marketing managers are the Alliance sales team single interface with/to any other GitLab marketing team**
    * Corporate events
    * Public Relations
    * Analyst relations
    * Brand
    * etc.

Our team is small but mighty and there are a few things that are simply **outside of our scope as of Sept 2022:** 
* **Content development**
   * Alliance marketing managers are responsible for ensuring alliances messaging is incorporated into company lightning strikes, but isn’t the author of that content.  Solution architects or other subject matter experts are best suited to author integration content.  
* **Enablement content**
   * Enablement is a sales and/or technical motion and the marketing manager can review or include the training as part of a larger initiative (and should) but would not be the one to author the content.  Alliances marketing manager can also lean on our enablement team to help provide resources/assistance.
* **Sales incentives**
   * Sales incentives are funded out of the Partner Operations team.  The alliances marketing manager will include the incentive as part of a larger initiative but would not be the one to author or fund the incentive.  
 


## Meet the Team
       
**Channel Marketing Team**

*   **Samara Souza**: Senior Partner Program Manager, Samara will be focusing on creating new service offerings for our partners to leverage in their demand generation motions including but not limited to: Free trial syndication; case study development, MDF process, and much much more! 
        
*   **Kelly Walker**: Senior Partner Program Manager, Exec Engagement, Kelly will be focusing on our channel partner events including GitLab Partner Summits and more!

*   **Lauren Roberts**: Partner Marketing Specialist,  Lauren will be focusing on supporting our channel partner programs including the Free trial program; partner microsites, and integrated [Instant Marketing Campaigns](https://about.gitlab.com/handbook/marketing/channel-marketing/instant-campaigns/#what-is-a-partner-instant-marketing-campaign) for GitLab channel partners.  

        
*   **Gabby Chorny**: Senior Channel Marketing Manager for AMER and Pub Sec.

*   **Daria Polukanina**: Channel Marketing Manager for EMEA and APAC


## The best way to contact our team is through our Slack channels

#channel-marketing 

